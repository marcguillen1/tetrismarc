package cat.escolapia.damviod.pmdm.GodTetris;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by marc_ on 05/01/2017.
 */

public class GameScreen extends Screen {

    enum GameState {
        Starting,
        Running,
        Paused,
        GameOver
    }

    GameState state = GameState.Starting;
    Board board;
    String score = "0";
    int _lastX;
    int _lastY;

    public GameScreen(Game game){
        super(game);
        board = new Board();
    }
    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Starting) {
            updateStarting(touchEvents);
        }
        if(state == GameState.Running) {
            updateRunning(touchEvents, deltaTime);
        }
        if(state == GameState.Paused) {
            updatePaused(touchEvents);
        }
        if(state == GameState.GameOver) {
            updateGameOver(touchEvents);
        }
    }

    private void updateGameOver(List<Input.TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    private void updatePaused(List<Input.TouchEvent> touchEvents) {
        int length = touchEvents.size();
        for(int i = 0; i < length; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {
                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateRunning(List<Input.TouchEvent> touchEvents, float deltaTime) {
        int length = touchEvents.size();
        for(int i = 0; i < length; i++)
        {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {
                int distX = event.x - _lastX;
                int distY = event.y - _lastY;
                if(Math.abs(distX) + Math.abs(distY) > 10) {
                    boolean verticalmoviment = false;
                    if (Math.abs(distX) < Math.abs(distY)) verticalmoviment = true;
                    if(verticalmoviment)
                    {
                        if(distY>0)board.currentPiece.pushDown();
                    }else{
                        if(distX>0)board.currentPiece.moveRight();
                        else board.currentPiece.moveLeft();
                    }
                }
                if(event.x > 0 && event.x < 45)
                {
                    if(event.y > 0 && event.y < 45)
                    {
                        state = GameState.Paused;
                    }
                }
            }
            if(event.type == Input.TouchEvent.TOUCH_DOWN) {
                _lastX = event.x;
                _lastY = event.y;
            }
        }
        board.update(deltaTime);
        score = Integer.toString(board.score);

        if(board.gameOver) {
            state = GameState.GameOver;
        }
    }

    private void updateStarting(List<Input.TouchEvent> touchEvents) {
    }


    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
