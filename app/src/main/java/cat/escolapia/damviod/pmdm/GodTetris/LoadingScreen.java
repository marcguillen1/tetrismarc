package cat.escolapia.damviod.pmdm.GodTetris;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

/**
 * Created by marc_ on 05/01/2017.
 */

public class LoadingScreen extends Screen{
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.startBg = g.newPixmap("startBackground.jpg", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("Logo.png", PixmapFormat.ARGB4444);
        Assets.playbutton = g.newPixmap("playbutton.png", PixmapFormat.ARGB4444);
        Assets.tile = g.newPixmap("tile.png", PixmapFormat.ARGB4444);
        game.setScreen(new MainMenuScreen(game));
    }

    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
