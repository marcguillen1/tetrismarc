package cat.escolapia.damviod.pmdm.GodTetris;

/**
 * Created by marc_ on 05/01/2017.
 */

import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap startBg;
    public static Pixmap logo;
    public static Pixmap playbutton;
    public static Pixmap tile;
}
