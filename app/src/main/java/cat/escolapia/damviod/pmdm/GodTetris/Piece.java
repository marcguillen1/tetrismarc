package cat.escolapia.damviod.pmdm.GodTetris;

/**
 * Created by marc_ on 05/01/2017.
 */

public class Piece {
    enum SHAPE_TYPE{
        T,
        I,
        L,
        S,
        R,
        SI,
        LI
    }

    public int x, y, type;
    public static int numShapes = 7;

    public Piece(int type)
    {

    }

    public void pushDown()
    {

    }

    public void moveRight()
    {

    }

    public void moveLeft()
    {

    }

    public void rotatePiece()
    {

    }

    public void moveDown()
    {

    }
}
