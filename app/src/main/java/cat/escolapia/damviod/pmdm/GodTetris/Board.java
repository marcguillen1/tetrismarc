package cat.escolapia.damviod.pmdm.GodTetris;

import java.util.Random;

/**
 * Created by marc_ on 05/01/2017.
 */

public class Board {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 16;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.04f;

    int map[][] = new int[WORLD_WIDTH][WORLD_HEIGHT];
    public static Piece currentPiece;
    public static int nextPieceType;
    public static int score = 0;
    public static Random random;
    public boolean gameOver = false;

    float tickTime = 0;
    float tick = TICK_INITIAL;

    public Board()
    {
        newPiece();
    }

    public void update(float deltatime)
    {
        if(gameOver) return;

        tickTime += deltatime;

        while(tickTime > tick)
        {
            if(checkCanMoveDown())
            {
                currentPiece.moveDown();
            }
        }

    }

    public boolean checkCanMoveDown()
    {
        return true;
    }

    public void newPiece()
    {

    }
}
